#include "ProxyDX9.h"

#if __has_include( <render/d3drender.h>)
#	include <render/d3drender.h>
#endif
#if __has_include( <render/texture.h>)
#	include <render/texture.h>
#endif

#if __has_include( <d3d9.h>)
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
namespace {
	template<typename T> bool deque_removeOne( std::deque<T> deq, const T &value ) noexcept {
		auto it = std::find( deq.begin(), deq.end(), value );
		if ( it == deq.end() ) return false;
		deq.erase( it );
		return true;
	}
} // namespace
#	endif

hookIDirect3DDevice9::hookIDirect3DDevice9( IDirect3DDevice9 *pOriginal ) noexcept {
	origIDirect3DDevice9 = pOriginal;
}

hookIDirect3DDevice9::~hookIDirect3DDevice9( void ) noexcept {
	d3d9_destroy();
	if ( *reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 ) == this )
		*reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 ) = origIDirect3DDevice9;
}

bool hookIDirect3DDevice9::d3d9_destroy() noexcept {
	if ( *reinterpret_cast<IDirect3DDevice9 **>( 0xC97C28 ) != this ) {
		*reinterpret_cast<size_t **>( this ) = *reinterpret_cast<size_t **>( origIDirect3DDevice9 );
		return false;
	}
	return true;
}

HRESULT hookIDirect3DDevice9::QueryInterface( REFIID riid, void **ppvObj ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onQueryInterface( riid, ppvObj );
#	endif
	*ppvObj = NULL;
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->QueryInterface( riid, ppvObj );
	if ( _hookResult.Hresult == NOERROR ) *ppvObj = this;
	return _hookResult.Hresult;
}

ULONG hookIDirect3DDevice9::AddRef() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onAddRef();
#	endif
	if ( !isHooked() ) _hookResult.Ulong = origIDirect3DDevice9->AddRef();
	return _hookResult.Ulong;
}

ULONG hookIDirect3DDevice9::Release() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onRelease();
#	endif
	if ( !isHooked() ) _hookResult.Ulong = origIDirect3DDevice9->Release();
	if ( _hookResult.Ulong == 0 ) {
		delete ( this );
		return 0;
	}
	return _hookResult.Ulong;
}

HRESULT hookIDirect3DDevice9::TestCooperativeLevel() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onTestCooperativeLevel();
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->TestCooperativeLevel();
	return _hookResult.Hresult;
}

UINT hookIDirect3DDevice9::GetAvailableTextureMem() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetAvailableTextureMem();
#	endif
	if ( !isHooked() ) _hookResult.Uint = origIDirect3DDevice9->GetAvailableTextureMem();
	return _hookResult.Uint;
}

HRESULT hookIDirect3DDevice9::EvictManagedResources() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onEvictManagedResources();
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->EvictManagedResources();
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetDirect3D( IDirect3D9 **ppD3D9 ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetDirect3D( ppD3D9 );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetDirect3D( ppD3D9 );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetDeviceCaps( D3DCAPS9 *pCaps ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetDeviceCaps( pCaps );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetDeviceCaps( pCaps );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetDisplayMode( UINT iSwapChain, D3DDISPLAYMODE *pMode ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetDisplayMode( iSwapChain, pMode );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetDisplayMode( iSwapChain, pMode );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetCreationParameters( D3DDEVICE_CREATION_PARAMETERS *pParameters ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetCreationParameters( pParameters );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetCreationParameters( pParameters );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetCursorProperties( UINT XHotSpot, UINT YHotSpot, IDirect3DSurface9 *pCursorBitmap ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetCursorProperties( XHotSpot, YHotSpot, pCursorBitmap );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetCursorProperties( XHotSpot, YHotSpot, pCursorBitmap );
	return _hookResult.Hresult;
}

void hookIDirect3DDevice9::SetCursorPosition( int X, int Y, DWORD Flags ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetCursorPosition( X, Y, Flags );
#	endif
	if ( !isHooked() ) return origIDirect3DDevice9->SetCursorPosition( X, Y, Flags );
}

BOOL hookIDirect3DDevice9::ShowCursor( BOOL bShow ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onShowCursor( bShow );
#	endif
	if ( !isHooked() ) _hookResult.Bool = origIDirect3DDevice9->ShowCursor( bShow );
	return _hookResult.Bool;
}

HRESULT hookIDirect3DDevice9::CreateAdditionalSwapChain( D3DPRESENT_PARAMETERS *pPresentationParameters,
														 IDirect3DSwapChain9 **pSwapChain ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateAdditionalSwapChain( pPresentationParameters, pSwapChain );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->CreateAdditionalSwapChain( pPresentationParameters, pSwapChain );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetSwapChain( UINT iSwapChain, IDirect3DSwapChain9 **pSwapChain ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetSwapChain( iSwapChain, pSwapChain );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetSwapChain( iSwapChain, pSwapChain );
	return _hookResult.Hresult;
}

UINT hookIDirect3DDevice9::GetNumberOfSwapChains() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetNumberOfSwapChains();
#	endif
	if ( !isHooked() ) _hookResult.Uint = origIDirect3DDevice9->GetNumberOfSwapChains();
	return _hookResult.Uint;
}

HRESULT hookIDirect3DDevice9::Reset( D3DPRESENT_PARAMETERS *pPresentationParameters ) noexcept {
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	for ( auto pFont : fontList ) {
		if ( pFont != nullptr ) pFont->Invalidate();
		// else fontList.removeOne(pFont);
		else
			deque_removeOne( fontList, pFont );
	}
	for ( auto pRender : renderList ) {
		if ( pRender != nullptr ) pRender->Invalidate();
		// else renderList.removeOne(pRender);
		else
			deque_removeOne( renderList, pRender );
	}
	for ( auto pTexture : textureList ) {
		if ( pTexture != nullptr ) pTexture->Invalidate();
		// else textureList.removeOne(pTexture);
		else
			deque_removeOne( textureList, pTexture );
	}
#	endif
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onPreReset();
#	endif

	resetHooked();
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onReset( pPresentationParameters );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->Reset( pPresentationParameters );
	if ( _hookResult.Hresult == D3D_OK ) {
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
		for ( auto pFont : fontList ) { pFont->Initialize( origIDirect3DDevice9 ); }
		for ( auto pRender : renderList ) { pRender->Initialize( origIDirect3DDevice9 ); }
		for ( auto pTexture : textureList ) { pTexture->Initialize( origIDirect3DDevice9 ); }
#	endif
#	if __has_include( <SRSignal/SRSignal.hpp>)
		onPostReset();
#	endif
	} else {
#	if __has_include( <SRSignal/SRSignal.hpp>)
		onPostResetFail( _hookResult.Hresult );
#	endif
	}
	return _hookResult.Hresult;
}

HRESULT
hookIDirect3DDevice9::Present( CONST RECT *pSourceRect,
							   CONST RECT *pDestRect,
							   HWND hDestWindowOverride,
							   CONST RGNDATA *pDirtyRegion ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onDraw();
#	endif
	//if ( g_class.cursor->isLocalCursorShowed() && g_class.cursor->isCursorHiden() ) g_class.cursor->showCursor();

	resetHooked();
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onPresent( pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->Present( pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion );
	return _hookResult.Hresult;
}

HRESULT
hookIDirect3DDevice9::GetBackBuffer( UINT iSwapChain,
									 UINT iBackBuffer,
									 D3DBACKBUFFER_TYPE Type,
									 IDirect3DSurface9 **ppBackBuffer ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetBackBuffer( iSwapChain, iBackBuffer, Type, ppBackBuffer );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetBackBuffer( iSwapChain, iBackBuffer, Type, ppBackBuffer );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetRasterStatus( UINT iSwapChain, D3DRASTER_STATUS *pRasterStatus ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetRasterStatus( iSwapChain, pRasterStatus );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetRasterStatus( iSwapChain, pRasterStatus );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetDialogBoxMode( BOOL bEnableDialogs ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetDialogBoxMode( bEnableDialogs );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetDialogBoxMode( bEnableDialogs );
	return _hookResult.Hresult;
}

void hookIDirect3DDevice9::SetGammaRamp( UINT iSwapChain, DWORD Flags, CONST D3DGAMMARAMP *pRamp ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetGammaRamp( iSwapChain, Flags, pRamp );
#	endif
	if ( !isHooked() ) return origIDirect3DDevice9->SetGammaRamp( iSwapChain, Flags, pRamp );
}

void hookIDirect3DDevice9::GetGammaRamp( UINT iSwapChain, D3DGAMMARAMP *pRamp ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetGammaRamp( iSwapChain, pRamp );
#	endif
	if ( !isHooked() ) return origIDirect3DDevice9->GetGammaRamp( iSwapChain, pRamp );
}

HRESULT hookIDirect3DDevice9::CreateTexture( UINT Width,
											 UINT Height,
											 UINT Levels,
											 DWORD Usage,
											 D3DFORMAT Format,
											 D3DPOOL Pool,
											 IDirect3DTexture9 **ppTexture,
											 HANDLE *pSharedHandle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateTexture( Width, Height, Levels, Usage, Format, Pool, ppTexture, pSharedHandle );
	if ( !isHooked() )
#	endif
		_hookResult.Hresult = origIDirect3DDevice9->CreateTexture( Width, Height, Levels, Usage, Format, Pool, ppTexture, pSharedHandle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateVolumeTexture( UINT Width,
												   UINT Height,
												   UINT Depth,
												   UINT Levels,
												   DWORD Usage,
												   D3DFORMAT Format,
												   D3DPOOL Pool,
												   IDirect3DVolumeTexture9 **ppVolumeTexture,
												   HANDLE *pSharedHandle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateVolumeTexture( Width, Height, Depth, Levels, Usage, Format, Pool, ppVolumeTexture, pSharedHandle );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult =
			origIDirect3DDevice9->CreateVolumeTexture( Width, Height, Depth, Levels, Usage, Format, Pool, ppVolumeTexture, pSharedHandle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateCubeTexture( UINT EdgeLength,
												 UINT Levels,
												 DWORD Usage,
												 D3DFORMAT Format,
												 D3DPOOL Pool,
												 IDirect3DCubeTexture9 **ppCubeTexture,
												 HANDLE *pSharedHandle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateCubeTexture( EdgeLength, Levels, Usage, Format, Pool, ppCubeTexture, pSharedHandle );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult =
			origIDirect3DDevice9->CreateCubeTexture( EdgeLength, Levels, Usage, Format, Pool, ppCubeTexture, pSharedHandle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateVertexBuffer( UINT Length,
												  DWORD Usage,
												  DWORD FVF,
												  D3DPOOL Pool,
												  IDirect3DVertexBuffer9 **ppVertexBuffer,
												  HANDLE *pSharedHandle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateVertexBuffer( Length, Usage, FVF, Pool, ppVertexBuffer, pSharedHandle );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult = origIDirect3DDevice9->CreateVertexBuffer( Length, Usage, FVF, Pool, ppVertexBuffer, pSharedHandle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateIndexBuffer( UINT Length,
												 DWORD Usage,
												 D3DFORMAT Format,
												 D3DPOOL Pool,
												 IDirect3DIndexBuffer9 **ppIndexBuffer,
												 HANDLE *pSharedHandle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateIndexBuffer( Length, Usage, Format, Pool, ppIndexBuffer, pSharedHandle );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult = origIDirect3DDevice9->CreateIndexBuffer( Length, Usage, Format, Pool, ppIndexBuffer, pSharedHandle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateRenderTarget( UINT Width,
												  UINT Height,
												  D3DFORMAT Format,
												  D3DMULTISAMPLE_TYPE MultiSample,
												  DWORD MultisampleQuality,
												  BOOL Lockable,
												  IDirect3DSurface9 **ppSurface,
												  HANDLE *pSharedHandle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateRenderTarget( Width, Height, Format, MultiSample, MultisampleQuality, Lockable, ppSurface, pSharedHandle );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult =
			origIDirect3DDevice9
				->CreateRenderTarget( Width, Height, Format, MultiSample, MultisampleQuality, Lockable, ppSurface, pSharedHandle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateDepthStencilSurface( UINT Width,
														 UINT Height,
														 D3DFORMAT Format,
														 D3DMULTISAMPLE_TYPE MultiSample,
														 DWORD MultisampleQuality,
														 BOOL Discard,
														 IDirect3DSurface9 **ppSurface,
														 HANDLE *pSharedHandle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateDepthStencilSurface( Width, Height, Format, MultiSample, MultisampleQuality, Discard, ppSurface, pSharedHandle );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult =
			origIDirect3DDevice9
				->CreateDepthStencilSurface( Width, Height, Format, MultiSample, MultisampleQuality, Discard, ppSurface, pSharedHandle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::UpdateSurface( IDirect3DSurface9 *pSourceSurface,
											 CONST RECT *pSourceRect,
											 IDirect3DSurface9 *pDestinationSurface,
											 CONST POINT *pDestPoint ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onUpdateSurface( pSourceSurface, pSourceRect, pDestinationSurface, pDestPoint );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult = origIDirect3DDevice9->UpdateSurface( pSourceSurface, pSourceRect, pDestinationSurface, pDestPoint );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::UpdateTexture( IDirect3DBaseTexture9 *pSourceTexture, IDirect3DBaseTexture9 *pDestinationTexture ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onUpdateTexture( pSourceTexture, pDestinationTexture );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->UpdateTexture( pSourceTexture, pDestinationTexture );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetRenderTargetData( IDirect3DSurface9 *pRenderTarget, IDirect3DSurface9 *pDestSurface ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetRenderTargetData( pRenderTarget, pDestSurface );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetRenderTargetData( pRenderTarget, pDestSurface );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetFrontBufferData( UINT iSwapChain, IDirect3DSurface9 *pDestSurface ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetFrontBufferData( iSwapChain, pDestSurface );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetFrontBufferData( iSwapChain, pDestSurface );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::StretchRect( IDirect3DSurface9 *pSourceSurface,
										   CONST RECT *pSourceRect,
										   IDirect3DSurface9 *pDestSurface,
										   CONST RECT *pDestRect,
										   D3DTEXTUREFILTERTYPE Filter ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onStretchRect( pSourceSurface, pSourceRect, pDestSurface, pDestRect, Filter );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult = origIDirect3DDevice9->StretchRect( pSourceSurface, pSourceRect, pDestSurface, pDestRect, Filter );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::ColorFill( IDirect3DSurface9 *pSurface, CONST RECT *pRect, D3DCOLOR color ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onColorFill( pSurface, pRect, color );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->ColorFill( pSurface, pRect, color );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateOffscreenPlainSurface( UINT Width,
														   UINT Height,
														   D3DFORMAT Format,
														   D3DPOOL Pool,
														   IDirect3DSurface9 **ppSurface,
														   HANDLE *pSharedHandle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateOffscreenPlainSurface( Width, Height, Format, Pool, ppSurface, pSharedHandle );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult = origIDirect3DDevice9->CreateOffscreenPlainSurface( Width, Height, Format, Pool, ppSurface, pSharedHandle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetRenderTarget( DWORD RenderTargetIndex, IDirect3DSurface9 *pRenderTarget ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetRenderTarget( RenderTargetIndex, pRenderTarget );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetRenderTarget( RenderTargetIndex, pRenderTarget );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetRenderTarget( DWORD RenderTargetIndex, IDirect3DSurface9 **ppRenderTarget ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetRenderTarget( RenderTargetIndex, ppRenderTarget );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetRenderTarget( RenderTargetIndex, ppRenderTarget );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetDepthStencilSurface( IDirect3DSurface9 *pNewZStencil ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetDepthStencilSurface( pNewZStencil );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetDepthStencilSurface( pNewZStencil );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetDepthStencilSurface( IDirect3DSurface9 **ppZStencilSurface ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetDepthStencilSurface( ppZStencilSurface );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetDepthStencilSurface( ppZStencilSurface );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::BeginScene() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onBeginScene();
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->BeginScene();
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::EndScene() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onEndScene();
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->EndScene();
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::Clear( DWORD Count, CONST D3DRECT *pRects, DWORD Flags, D3DCOLOR Color, float Z, DWORD Stencil ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onClear( Count, pRects, Flags, Color, Z, Stencil );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->Clear( Count, pRects, Flags, Color, Z, Stencil );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetTransform( D3DTRANSFORMSTATETYPE State, CONST D3DMATRIX *pMatrix ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetTransform( State, pMatrix );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetTransform( State, pMatrix );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetTransform( D3DTRANSFORMSTATETYPE State, D3DMATRIX *pMatrix ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetTransform( State, pMatrix );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetTransform( State, pMatrix );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::MultiplyTransform( D3DTRANSFORMSTATETYPE State, CONST D3DMATRIX *pMatrix ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onMultiplyTransform( State, pMatrix );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->MultiplyTransform( State, pMatrix );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetViewport( CONST D3DVIEWPORT9 *pViewport ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetViewport( pViewport );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetViewport( pViewport );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetViewport( D3DVIEWPORT9 *pViewport ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetViewport( pViewport );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetViewport( pViewport );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetMaterial( CONST D3DMATERIAL9 *pMaterial ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetMaterial( pMaterial );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetMaterial( pMaterial );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetMaterial( D3DMATERIAL9 *pMaterial ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetMaterial( pMaterial );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetMaterial( pMaterial );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetLight( DWORD Index, CONST D3DLIGHT9 *pLight ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetLight( Index, pLight );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetLight( Index, pLight );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetLight( DWORD Index, D3DLIGHT9 *pLight ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetLight( Index, pLight );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetLight( Index, pLight );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::LightEnable( DWORD Index, BOOL Enable ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onLightEnable( Index, Enable );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->LightEnable( Index, Enable );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetLightEnable( DWORD Index, BOOL *pEnable ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetLightEnable( Index, pEnable );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetLightEnable( Index, pEnable );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetClipPlane( DWORD Index, CONST float *pPlane ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetClipPlane( Index, pPlane );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetClipPlane( Index, pPlane );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetClipPlane( DWORD Index, float *pPlane ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetClipPlane( Index, pPlane );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetClipPlane( Index, pPlane );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetRenderState( D3DRENDERSTATETYPE State, DWORD Value ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetRenderState( State, Value );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetRenderState( State, Value );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetRenderState( D3DRENDERSTATETYPE State, DWORD *pValue ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetRenderState( State, pValue );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetRenderState( State, pValue );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateStateBlock( D3DSTATEBLOCKTYPE Type, IDirect3DStateBlock9 **ppSB ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateStateBlock( Type, ppSB );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->CreateStateBlock( Type, ppSB );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::BeginStateBlock() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onBeginStateBlock();
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->BeginStateBlock();
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::EndStateBlock( IDirect3DStateBlock9 **ppSB ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onEndStateBlock( ppSB );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->EndStateBlock( ppSB );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetClipStatus( CONST D3DCLIPSTATUS9 *pClipStatus ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetClipStatus( pClipStatus );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetClipStatus( pClipStatus );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetClipStatus( D3DCLIPSTATUS9 *pClipStatus ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetClipStatus( pClipStatus );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetClipStatus( pClipStatus );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetTexture( DWORD Stage, IDirect3DBaseTexture9 **ppTexture ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetTexture( Stage, ppTexture );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetTexture( Stage, ppTexture );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetTexture( DWORD Stage, IDirect3DBaseTexture9 *pTexture ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetTexture( Stage, pTexture );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetTexture( Stage, pTexture );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetTextureStageState( DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD *pValue ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetTextureStageState( Stage, Type, pValue );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetTextureStageState( Stage, Type, pValue );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetTextureStageState( DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD Value ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetTextureStageState( Stage, Type, Value );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetTextureStageState( Stage, Type, Value );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetSamplerState( DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD *pValue ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetSamplerState( Sampler, Type, pValue );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetSamplerState( Sampler, Type, pValue );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetSamplerState( DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD Value ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetSamplerState( Sampler, Type, Value );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetSamplerState( Sampler, Type, Value );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::ValidateDevice( DWORD *pNumPasses ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onValidateDevice( pNumPasses );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->ValidateDevice( pNumPasses );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetPaletteEntries( UINT PaletteNumber, CONST PALETTEENTRY *pEntries ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetPaletteEntries( PaletteNumber, pEntries );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetPaletteEntries( PaletteNumber, pEntries );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetPaletteEntries( UINT PaletteNumber, PALETTEENTRY *pEntries ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetPaletteEntries( PaletteNumber, pEntries );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetPaletteEntries( PaletteNumber, pEntries );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetCurrentTexturePalette( UINT PaletteNumber ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetCurrentTexturePalette( PaletteNumber );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetCurrentTexturePalette( PaletteNumber );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetCurrentTexturePalette( UINT *PaletteNumber ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetCurrentTexturePalette( PaletteNumber );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetCurrentTexturePalette( PaletteNumber );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetScissorRect( CONST RECT *pRect ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetScissorRect( pRect );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetScissorRect( pRect );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetScissorRect( RECT *pRect ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetScissorRect( pRect );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetScissorRect( pRect );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetSoftwareVertexProcessing( BOOL bSoftware ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetSoftwareVertexProcessing( bSoftware );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetSoftwareVertexProcessing( bSoftware );
	return _hookResult.Hresult;
}

BOOL hookIDirect3DDevice9::GetSoftwareVertexProcessing() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetSoftwareVertexProcessing();
#	endif
	if ( !isHooked() ) _hookResult.Bool = origIDirect3DDevice9->GetSoftwareVertexProcessing();
	return _hookResult.Bool;
}

HRESULT hookIDirect3DDevice9::SetNPatchMode( float nSegments ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetNPatchMode( nSegments );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetNPatchMode( nSegments );
	return _hookResult.Hresult;
}

float hookIDirect3DDevice9::GetNPatchMode() noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetNPatchMode();
#	endif
	if ( !isHooked() ) _hookResult.Float = origIDirect3DDevice9->GetNPatchMode();
	return _hookResult.Float;
}

HRESULT hookIDirect3DDevice9::DrawPrimitive( D3DPRIMITIVETYPE PrimitiveType, UINT StartVertex, UINT PrimitiveCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onDrawPrimitive( PrimitiveType, StartVertex, PrimitiveCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->DrawPrimitive( PrimitiveType, StartVertex, PrimitiveCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::DrawIndexedPrimitive( D3DPRIMITIVETYPE PrimitiveType,
													INT BaseVertexIndex,
													UINT MinVertexIndex,
													UINT NumVertices,
													UINT startIndex,
													UINT primCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onDrawIndexedPrimitive( PrimitiveType, BaseVertexIndex, MinVertexIndex, NumVertices, startIndex, primCount );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult = origIDirect3DDevice9->DrawIndexedPrimitive( PrimitiveType,
																		  BaseVertexIndex,
																		  MinVertexIndex,
																		  NumVertices,
																		  startIndex,
																		  primCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::DrawPrimitiveUP( D3DPRIMITIVETYPE PrimitiveType,
											   UINT PrimitiveCount,
											   CONST void *pVertexStreamZeroData,
											   UINT VertexStreamZeroStride ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onDrawPrimitiveUP( PrimitiveType, PrimitiveCount, pVertexStreamZeroData, VertexStreamZeroStride );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult =
			origIDirect3DDevice9->DrawPrimitiveUP( PrimitiveType, PrimitiveCount, pVertexStreamZeroData, VertexStreamZeroStride );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::DrawIndexedPrimitiveUP( D3DPRIMITIVETYPE PrimitiveType,
													  UINT MinVertexIndex,
													  UINT NumVertices,
													  UINT PrimitiveCount,
													  CONST void *pIndexData,
													  D3DFORMAT IndexDataFormat,
													  CONST void *pVertexStreamZeroData,
													  UINT VertexStreamZeroStride ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onDrawIndexedPrimitiveUP( PrimitiveType,
							  MinVertexIndex,
							  NumVertices,
							  PrimitiveCount,
							  pIndexData,
							  IndexDataFormat,
							  pVertexStreamZeroData,
							  VertexStreamZeroStride );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult = origIDirect3DDevice9->DrawIndexedPrimitiveUP( PrimitiveType,
																			MinVertexIndex,
																			NumVertices,
																			PrimitiveCount,
																			pIndexData,
																			IndexDataFormat,
																			pVertexStreamZeroData,
																			VertexStreamZeroStride );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::ProcessVertices( UINT SrcStartIndex,
											   UINT DestIndex,
											   UINT VertexCount,
											   IDirect3DVertexBuffer9 *pDestBuffer,
											   IDirect3DVertexDeclaration9 *pVertexDecl,
											   DWORD Flags ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onProcessVertices( SrcStartIndex, DestIndex, VertexCount, pDestBuffer, pVertexDecl, Flags );
#	endif
	if ( !isHooked() )
		_hookResult.Hresult =
			origIDirect3DDevice9->ProcessVertices( SrcStartIndex, DestIndex, VertexCount, pDestBuffer, pVertexDecl, Flags );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateVertexDeclaration( CONST D3DVERTEXELEMENT9 *pVertexElements,
													   IDirect3DVertexDeclaration9 **ppDecl ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateVertexDeclaration( pVertexElements, ppDecl );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->CreateVertexDeclaration( pVertexElements, ppDecl );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetVertexDeclaration( IDirect3DVertexDeclaration9 *pDecl ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetVertexDeclaration( pDecl );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetVertexDeclaration( pDecl );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetVertexDeclaration( IDirect3DVertexDeclaration9 **ppDecl ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetVertexDeclaration( ppDecl );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetVertexDeclaration( ppDecl );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetFVF( DWORD FVF ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetFVF( FVF );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetFVF( FVF );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetFVF( DWORD *pFVF ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetFVF( pFVF );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetFVF( pFVF );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateVertexShader( CONST DWORD *pFunction, IDirect3DVertexShader9 **ppShader ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateVertexShader( pFunction, ppShader );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->CreateVertexShader( pFunction, ppShader );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetVertexShader( IDirect3DVertexShader9 *pShader ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetVertexShader( pShader );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetVertexShader( pShader );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetVertexShader( IDirect3DVertexShader9 **ppShader ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetVertexShader( ppShader );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetVertexShader( ppShader );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetVertexShaderConstantF( UINT StartRegister, CONST float *pConstantData, UINT Vector4fCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetVertexShaderConstantF( StartRegister, pConstantData, Vector4fCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetVertexShaderConstantF( StartRegister, pConstantData, Vector4fCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetVertexShaderConstantF( UINT StartRegister, float *pConstantData, UINT Vector4fCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetVertexShaderConstantF( StartRegister, pConstantData, Vector4fCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetVertexShaderConstantF( StartRegister, pConstantData, Vector4fCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetVertexShaderConstantI( UINT StartRegister, CONST int *pConstantData, UINT Vector4iCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetVertexShaderConstantI( StartRegister, pConstantData, Vector4iCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetVertexShaderConstantI( StartRegister, pConstantData, Vector4iCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetVertexShaderConstantI( UINT StartRegister, int *pConstantData, UINT Vector4iCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetVertexShaderConstantI( StartRegister, pConstantData, Vector4iCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetVertexShaderConstantI( StartRegister, pConstantData, Vector4iCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetVertexShaderConstantB( UINT StartRegister, CONST BOOL *pConstantData, UINT BoolCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetVertexShaderConstantB( StartRegister, pConstantData, BoolCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetVertexShaderConstantB( StartRegister, pConstantData, BoolCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetVertexShaderConstantB( UINT StartRegister, BOOL *pConstantData, UINT BoolCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetVertexShaderConstantB( StartRegister, pConstantData, BoolCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetVertexShaderConstantB( StartRegister, pConstantData, BoolCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetStreamSource( UINT StreamNumber,
											   IDirect3DVertexBuffer9 *pStreamData,
											   UINT OffsetInBytes,
											   UINT Stride ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetStreamSource( StreamNumber, pStreamData, OffsetInBytes, Stride );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetStreamSource( StreamNumber, pStreamData, OffsetInBytes, Stride );
	return _hookResult.Hresult;
}

HRESULT
hookIDirect3DDevice9::GetStreamSource( UINT StreamNumber,
									   IDirect3DVertexBuffer9 **ppStreamData,
									   UINT *OffsetInBytes,
									   UINT *pStride ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetStreamSource( StreamNumber, ppStreamData, OffsetInBytes, pStride );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetStreamSource( StreamNumber, ppStreamData, OffsetInBytes, pStride );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetStreamSourceFreq( UINT StreamNumber, UINT Divider ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetStreamSourceFreq( StreamNumber, Divider );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetStreamSourceFreq( StreamNumber, Divider );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetStreamSourceFreq( UINT StreamNumber, UINT *Divider ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetStreamSourceFreq( StreamNumber, Divider );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetStreamSourceFreq( StreamNumber, Divider );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetIndices( IDirect3DIndexBuffer9 *pIndexData ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetIndices( pIndexData );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetIndices( pIndexData );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetIndices( IDirect3DIndexBuffer9 **ppIndexData ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetIndices( ppIndexData );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetIndices( ppIndexData );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreatePixelShader( CONST DWORD *pFunction, IDirect3DPixelShader9 **ppShader ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreatePixelShader( pFunction, ppShader );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->CreatePixelShader( pFunction, ppShader );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetPixelShader( IDirect3DPixelShader9 *pShader ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetPixelShader( pShader );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetPixelShader( pShader );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetPixelShader( IDirect3DPixelShader9 **ppShader ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetPixelShader( ppShader );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetPixelShader( ppShader );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetPixelShaderConstantF( UINT StartRegister, CONST float *pConstantData, UINT Vector4fCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetPixelShaderConstantF( StartRegister, pConstantData, Vector4fCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetPixelShaderConstantF( StartRegister, pConstantData, Vector4fCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetPixelShaderConstantF( UINT StartRegister, float *pConstantData, UINT Vector4fCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetPixelShaderConstantF( StartRegister, pConstantData, Vector4fCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetPixelShaderConstantF( StartRegister, pConstantData, Vector4fCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetPixelShaderConstantI( UINT StartRegister, CONST int *pConstantData, UINT Vector4iCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetPixelShaderConstantI( StartRegister, pConstantData, Vector4iCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetPixelShaderConstantI( StartRegister, pConstantData, Vector4iCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetPixelShaderConstantI( UINT StartRegister, int *pConstantData, UINT Vector4iCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetPixelShaderConstantI( StartRegister, pConstantData, Vector4iCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetPixelShaderConstantI( StartRegister, pConstantData, Vector4iCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::SetPixelShaderConstantB( UINT StartRegister, CONST BOOL *pConstantData, UINT BoolCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onSetPixelShaderConstantB( StartRegister, pConstantData, BoolCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->SetPixelShaderConstantB( StartRegister, pConstantData, BoolCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::GetPixelShaderConstantB( UINT StartRegister, BOOL *pConstantData, UINT BoolCount ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onGetPixelShaderConstantB( StartRegister, pConstantData, BoolCount );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->GetPixelShaderConstantB( StartRegister, pConstantData, BoolCount );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::DrawRectPatch( UINT Handle, CONST float *pNumSegs, CONST D3DRECTPATCH_INFO *pRectPatchInfo ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onDrawRectPatch( Handle, pNumSegs, pRectPatchInfo );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->DrawRectPatch( Handle, pNumSegs, pRectPatchInfo );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::DrawTriPatch( UINT Handle, CONST float *pNumSegs, CONST D3DTRIPATCH_INFO *pTriPatchInfo ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onDrawTriPatch( Handle, pNumSegs, pTriPatchInfo );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->DrawTriPatch( Handle, pNumSegs, pTriPatchInfo );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::DeletePatch( UINT Handle ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onDeletePatch( Handle );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->DeletePatch( Handle );
	return _hookResult.Hresult;
}

HRESULT hookIDirect3DDevice9::CreateQuery( D3DQUERYTYPE Type, IDirect3DQuery9 **ppQuery ) noexcept {
#	if __has_include( <SRSignal/SRSignal.hpp>)
	onCreateQuery( Type, ppQuery );
#	endif
	if ( !isHooked() ) _hookResult.Hresult = origIDirect3DDevice9->CreateQuery( Type, ppQuery );
	return _hookResult.Hresult;
}

void hookIDirect3DDevice9::d3d9_hook( uIDirect3DDevice9 ret ) {
	_hookResult = ret;
	_isHook = true;
}

bool hookIDirect3DDevice9::d3d9_hasHooked() {
	return isHooked();
}

bool hookIDirect3DDevice9::isHooked() {
	bool ret = _isHook;
	_isHook = false;
	return ret;
}

void hookIDirect3DDevice9::resetHooked() {
	_isHook = false;
}

IDirect3DDevice9 *hookIDirect3DDevice9::d3d9_device() {
	return origIDirect3DDevice9;
}

void hookIDirect3DDevice9::d3d9_hook( HRESULT ret ) {
	_hookResult.Hresult = ret;
	_isHook = true;
}

void hookIDirect3DDevice9::d3d9_hook( ULONG ret ) {
	_hookResult.Ulong = ret;
	_isHook = true;
}

void hookIDirect3DDevice9::d3d9_hook( UINT ret ) {
	_hookResult.Uint = ret;
	_isHook = true;
}

void hookIDirect3DDevice9::d3d9_hook( BOOL ret ) {
	_hookResult.Bool = ret;
	_isHook = true;
}

void hookIDirect3DDevice9::d3d9_hook( float ret ) {
	_hookResult.Float = ret;
	_isHook = true;
}

#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
CD3DFont *hookIDirect3DDevice9::d3d9_createFont( std::string_view fontName, int fontHeight, DWORD dwCreateFlags, DWORD dwCharSet ) {
	CD3DFont *pFont = new CD3DFont( fontName, fontHeight, dwCreateFlags, dwCharSet );
	d3d9_registerFont( pFont );
	return pFont;
}

CD3DRender *hookIDirect3DDevice9::d3d9_createRender( int numVertices ) {
	CD3DRender *pRender = new CD3DRender( numVertices );
	d3d9_registerRender( pRender );
	return pRender;
}

SRTexture *hookIDirect3DDevice9::d3d9_createTexture( int width, int height ) {
	SRTexture *pTexture = new SRTexture( width, height );
	d3d9_registerTexture( pTexture );
	return pTexture;
}

void hookIDirect3DDevice9::d3d9_registerFont( CD3DFont *pFont ) {
	fontList.push_back( pFont );
	pFont->Initialize( this );
}

void hookIDirect3DDevice9::d3d9_releaseFont( CD3DFont *pFont, bool autoDelete ) {
	if ( pFont == nullptr ) return;

	pFont->Invalidate();
	// fontList.removeOne(pFont);
	if ( deque_removeOne( fontList, pFont ) ) {
		if ( autoDelete ) {
			delete pFont;
			pFont = nullptr;
		}
	}
}

void hookIDirect3DDevice9::d3d9_registerRender( CD3DRender *pRender ) {
	renderList.push_back( pRender );
	pRender->Initialize( this );
}

void hookIDirect3DDevice9::d3d9_releaseRender( CD3DRender *pRender, bool autoDelete ) {
	if ( pRender == nullptr ) return;

	pRender->Invalidate();
	// renderList.removeOne(pRender);
	if ( deque_removeOne( renderList, pRender ) ) {
		if ( autoDelete ) {
			delete pRender;
			pRender = nullptr;
		}
	}
}

void hookIDirect3DDevice9::d3d9_registerTexture( SRTexture *pTexture ) {
	textureList.push_back( pTexture );
	pTexture->Initialize( this );
}

void hookIDirect3DDevice9::d3d9_releaseTexture( SRTexture *pTexture, bool autoDelete ) {
	if ( pTexture == nullptr ) return;

	// textureList.removeOne(pTexture);
	if ( deque_removeOne( textureList, pTexture ) ) {
		if ( autoDelete ) {
			delete pTexture;
			pTexture = nullptr;
		}
	}
}
#	endif
#endif
