#pragma once

#if __has_include( <SRSignal/SRSignal.hpp>)
#	include <SRSignal/SRSignal.hpp>
#else
#	include <deque>
#endif
#include <string_view>
#if __has_include( <d3d9.h>)
#	include <d3d9.h>

union uIDirect3DDevice9 {
	HRESULT Hresult;
	ULONG Ulong;
	UINT Uint;
	BOOL Bool;
	float Float;
};

#	ifdef _MSVC_VER
struct hookIDirect3DDevice9 : public IDirect3DDevice9 {
#	else
class hookIDirect3DDevice9 : public IDirect3DDevice9 {
#	endif
public:
	hookIDirect3DDevice9( IDirect3DDevice9 *pOriginal ) noexcept;
	virtual ~hookIDirect3DDevice9( void ) noexcept;

protected:
	HRESULT __stdcall QueryInterface( REFIID riid, void **ppvObj ) noexcept override;
	ULONG __stdcall AddRef( void ) noexcept override;
	ULONG __stdcall Release( void ) noexcept override;
	HRESULT __stdcall TestCooperativeLevel( void ) noexcept override;
	UINT __stdcall GetAvailableTextureMem( void ) noexcept override;
	HRESULT __stdcall EvictManagedResources( void ) noexcept override;
	HRESULT __stdcall GetDirect3D( IDirect3D9 **ppD3D9 ) noexcept override;
	HRESULT __stdcall GetDeviceCaps( D3DCAPS9 *pCaps ) noexcept override;
	HRESULT __stdcall GetDisplayMode( UINT iSwapChain, D3DDISPLAYMODE *pMode ) noexcept override;
	HRESULT __stdcall GetCreationParameters( D3DDEVICE_CREATION_PARAMETERS *pParameters ) noexcept override;
	HRESULT __stdcall SetCursorProperties( UINT XHotSpot, UINT YHotSpot, IDirect3DSurface9 *pCursorBitmap ) noexcept override;
	void __stdcall SetCursorPosition( int X, int Y, DWORD Flags ) noexcept override;
	BOOL __stdcall ShowCursor( BOOL bShow ) noexcept override;
	HRESULT __stdcall CreateAdditionalSwapChain( D3DPRESENT_PARAMETERS *pPresentationParameters,
												 IDirect3DSwapChain9 **pSwapChain ) noexcept override;
	HRESULT __stdcall GetSwapChain( UINT iSwapChain, IDirect3DSwapChain9 **pSwapChain ) noexcept override;
	UINT __stdcall GetNumberOfSwapChains( void ) noexcept override;
	HRESULT __stdcall Reset( D3DPRESENT_PARAMETERS *pPresentationParameters ) noexcept override;
	HRESULT __stdcall Present( CONST RECT *pSourceRect,
							   CONST RECT *pDestRect,
							   HWND hDestWindowOverride,
							   CONST RGNDATA *pDirtyRegion ) noexcept override;
	HRESULT __stdcall GetBackBuffer( UINT iSwapChain,
									 UINT iBackBuffer,
									 D3DBACKBUFFER_TYPE Type,
									 IDirect3DSurface9 **ppBackBuffer ) noexcept override;
	HRESULT __stdcall GetRasterStatus( UINT iSwapChain, D3DRASTER_STATUS *pRasterStatus ) noexcept override;
	HRESULT __stdcall SetDialogBoxMode( BOOL bEnableDialogs ) noexcept override;
	void __stdcall SetGammaRamp( UINT iSwapChain, DWORD Flags, CONST D3DGAMMARAMP *pRamp ) noexcept override;
	void __stdcall GetGammaRamp( UINT iSwapChain, D3DGAMMARAMP *pRamp ) noexcept override;
	HRESULT __stdcall CreateTexture( UINT Width,
									 UINT Height,
									 UINT Levels,
									 DWORD Usage,
									 D3DFORMAT Format,
									 D3DPOOL Pool,
									 IDirect3DTexture9 **ppTexture,
									 HANDLE *pSharedHandle ) noexcept override;
	HRESULT __stdcall CreateVolumeTexture( UINT Width,
										   UINT Height,
										   UINT Depth,
										   UINT Levels,
										   DWORD Usage,
										   D3DFORMAT Format,
										   D3DPOOL Pool,
										   IDirect3DVolumeTexture9 **ppVolumeTexture,
										   HANDLE *pSharedHandle ) noexcept override;
	HRESULT __stdcall CreateCubeTexture( UINT EdgeLength,
										 UINT Levels,
										 DWORD Usage,
										 D3DFORMAT Format,
										 D3DPOOL Pool,
										 IDirect3DCubeTexture9 **ppCubeTexture,
										 HANDLE *pSharedHandle ) noexcept override;
	HRESULT __stdcall CreateVertexBuffer( UINT Length,
										  DWORD Usage,
										  DWORD FVF,
										  D3DPOOL Pool,
										  IDirect3DVertexBuffer9 **ppVertexBuffer,
										  HANDLE *pSharedHandle ) noexcept override;
	HRESULT __stdcall CreateIndexBuffer( UINT Length,
										 DWORD Usage,
										 D3DFORMAT Format,
										 D3DPOOL Pool,
										 IDirect3DIndexBuffer9 **ppIndexBuffer,
										 HANDLE *pSharedHandle ) noexcept override;
	HRESULT __stdcall CreateRenderTarget( UINT Width,
										  UINT Height,
										  D3DFORMAT Format,
										  D3DMULTISAMPLE_TYPE MultiSample,
										  DWORD MultisampleQuality,
										  BOOL Lockable,
										  IDirect3DSurface9 **ppSurface,
										  HANDLE *pSharedHandle ) noexcept override;
	HRESULT __stdcall CreateDepthStencilSurface( UINT Width,
												 UINT Height,
												 D3DFORMAT Format,
												 D3DMULTISAMPLE_TYPE MultiSample,
												 DWORD MultisampleQuality,
												 BOOL Discard,
												 IDirect3DSurface9 **ppSurface,
												 HANDLE *pSharedHandle ) noexcept override;
	HRESULT __stdcall UpdateSurface( IDirect3DSurface9 *pSourceSurface,
									 CONST RECT *pSourceRect,
									 IDirect3DSurface9 *pDestinationSurface,
									 CONST POINT *pDestPoint ) noexcept override;
	HRESULT __stdcall UpdateTexture( IDirect3DBaseTexture9 *pSourceTexture, IDirect3DBaseTexture9 *pDestinationTexture ) noexcept override;
	HRESULT __stdcall GetRenderTargetData( IDirect3DSurface9 *pRenderTarget, IDirect3DSurface9 *pDestSurface ) noexcept override;
	HRESULT __stdcall GetFrontBufferData( UINT iSwapChain, IDirect3DSurface9 *pDestSurface ) noexcept override;
	HRESULT __stdcall StretchRect( IDirect3DSurface9 *pSourceSurface,
								   CONST RECT *pSourceRect,
								   IDirect3DSurface9 *pDestSurface,
								   CONST RECT *pDestRect,
								   D3DTEXTUREFILTERTYPE Filter ) noexcept override;
	HRESULT __stdcall ColorFill( IDirect3DSurface9 *pSurface, CONST RECT *pRect, D3DCOLOR color ) noexcept override;
	HRESULT __stdcall CreateOffscreenPlainSurface( UINT Width,
												   UINT Height,
												   D3DFORMAT Format,
												   D3DPOOL Pool,
												   IDirect3DSurface9 **ppSurface,
												   HANDLE *pSharedHandle ) noexcept override;
	HRESULT __stdcall SetRenderTarget( DWORD RenderTargetIndex, IDirect3DSurface9 *pRenderTarget ) noexcept override;
	HRESULT __stdcall GetRenderTarget( DWORD RenderTargetIndex, IDirect3DSurface9 **ppRenderTarget ) noexcept override;
	HRESULT __stdcall SetDepthStencilSurface( IDirect3DSurface9 *pNewZStencil ) noexcept override;
	HRESULT __stdcall GetDepthStencilSurface( IDirect3DSurface9 **ppZStencilSurface ) noexcept override;
	HRESULT __stdcall BeginScene( void ) noexcept override;
	HRESULT __stdcall EndScene( void ) noexcept override;
	HRESULT __stdcall Clear( DWORD Count, CONST D3DRECT *pRects, DWORD Flags, D3DCOLOR Color, float Z, DWORD Stencil ) noexcept override;
	HRESULT __stdcall SetTransform( D3DTRANSFORMSTATETYPE State, CONST D3DMATRIX *pMatrix ) noexcept override;
	HRESULT __stdcall GetTransform( D3DTRANSFORMSTATETYPE State, D3DMATRIX *pMatrix ) noexcept override;
	HRESULT __stdcall MultiplyTransform( D3DTRANSFORMSTATETYPE State, CONST D3DMATRIX *pMatrix ) noexcept override;
	HRESULT __stdcall SetViewport( CONST D3DVIEWPORT9 *pViewport ) noexcept override;
	HRESULT __stdcall GetViewport( D3DVIEWPORT9 *pViewport ) noexcept override;
	HRESULT __stdcall SetMaterial( CONST D3DMATERIAL9 *pMaterial ) noexcept override;
	HRESULT __stdcall GetMaterial( D3DMATERIAL9 *pMaterial ) noexcept override;
	HRESULT __stdcall SetLight( DWORD Index, CONST D3DLIGHT9 *pLight ) noexcept override;
	HRESULT __stdcall GetLight( DWORD Index, D3DLIGHT9 *pLight ) noexcept override;
	HRESULT __stdcall LightEnable( DWORD Index, BOOL Enable ) noexcept override;
	HRESULT __stdcall GetLightEnable( DWORD Index, BOOL *pEnable ) noexcept override;
	HRESULT __stdcall SetClipPlane( DWORD Index, CONST float *pPlane ) noexcept override;
	HRESULT __stdcall GetClipPlane( DWORD Index, float *pPlane ) noexcept override;
	HRESULT __stdcall SetRenderState( D3DRENDERSTATETYPE State, DWORD Value ) noexcept override;
	HRESULT __stdcall GetRenderState( D3DRENDERSTATETYPE State, DWORD *pValue ) noexcept override;
	HRESULT __stdcall CreateStateBlock( D3DSTATEBLOCKTYPE Type, IDirect3DStateBlock9 **ppSB ) noexcept override;
	HRESULT __stdcall BeginStateBlock( void ) noexcept override;
	HRESULT __stdcall EndStateBlock( IDirect3DStateBlock9 **ppSB ) noexcept override;
	HRESULT __stdcall SetClipStatus( CONST D3DCLIPSTATUS9 *pClipStatus ) noexcept override;
	HRESULT __stdcall GetClipStatus( D3DCLIPSTATUS9 *pClipStatus ) noexcept override;
	HRESULT __stdcall GetTexture( DWORD Stage, IDirect3DBaseTexture9 **ppTexture ) noexcept override;
	HRESULT __stdcall SetTexture( DWORD Stage, IDirect3DBaseTexture9 *pTexture ) noexcept override;
	HRESULT __stdcall GetTextureStageState( DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD *pValue ) noexcept override;
	HRESULT __stdcall SetTextureStageState( DWORD Stage, D3DTEXTURESTAGESTATETYPE Type, DWORD Value ) noexcept override;
	HRESULT __stdcall GetSamplerState( DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD *pValue ) noexcept override;
	HRESULT __stdcall SetSamplerState( DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD Value ) noexcept override;
	HRESULT __stdcall ValidateDevice( DWORD *pNumPasses ) noexcept override;
	HRESULT __stdcall SetPaletteEntries( UINT PaletteNumber, CONST PALETTEENTRY *pEntries ) noexcept override;
	HRESULT __stdcall GetPaletteEntries( UINT PaletteNumber, PALETTEENTRY *pEntries ) noexcept override;
	HRESULT __stdcall SetCurrentTexturePalette( UINT PaletteNumber ) noexcept override;
	HRESULT __stdcall GetCurrentTexturePalette( UINT *PaletteNumber ) noexcept override;
	HRESULT __stdcall SetScissorRect( CONST RECT *pRect ) noexcept override;
	HRESULT __stdcall GetScissorRect( RECT *pRect ) noexcept override;
	HRESULT __stdcall SetSoftwareVertexProcessing( BOOL bSoftware ) noexcept override;
	BOOL __stdcall GetSoftwareVertexProcessing( void ) noexcept override;
	HRESULT __stdcall SetNPatchMode( float nSegments ) noexcept override;
	float __stdcall GetNPatchMode( void ) noexcept override;
	HRESULT __stdcall DrawPrimitive( D3DPRIMITIVETYPE PrimitiveType, UINT StartVertex, UINT PrimitiveCount ) noexcept override;
	HRESULT __stdcall DrawIndexedPrimitive( D3DPRIMITIVETYPE PrimitiveType,
											INT BaseVertexIndex,
											UINT MinVertexIndex,
											UINT NumVertices,
											UINT startIndex,
											UINT primCount ) noexcept override;
	HRESULT __stdcall DrawPrimitiveUP( D3DPRIMITIVETYPE PrimitiveType,
									   UINT PrimitiveCount,
									   CONST void *pVertexStreamZeroData,
									   UINT VertexStreamZeroStride ) noexcept override;
	HRESULT __stdcall DrawIndexedPrimitiveUP( D3DPRIMITIVETYPE PrimitiveType,
											  UINT MinVertexIndex,
											  UINT NumVertices,
											  UINT PrimitiveCount,
											  CONST void *pIndexData,
											  D3DFORMAT IndexDataFormat,
											  CONST void *pVertexStreamZeroData,
											  UINT VertexStreamZeroStride ) noexcept override;
	HRESULT __stdcall ProcessVertices( UINT SrcStartIndex,
									   UINT DestIndex,
									   UINT VertexCount,
									   IDirect3DVertexBuffer9 *pDestBuffer,
									   IDirect3DVertexDeclaration9 *pVertexDecl,
									   DWORD Flags ) noexcept override;
	HRESULT __stdcall CreateVertexDeclaration( CONST D3DVERTEXELEMENT9 *pVertexElements,
											   IDirect3DVertexDeclaration9 **ppDecl ) noexcept override;
	HRESULT __stdcall SetVertexDeclaration( IDirect3DVertexDeclaration9 *pDecl ) noexcept override;
	HRESULT __stdcall GetVertexDeclaration( IDirect3DVertexDeclaration9 **ppDecl ) noexcept override;
	HRESULT __stdcall SetFVF( DWORD FVF ) noexcept override;
	HRESULT __stdcall GetFVF( DWORD *pFVF ) noexcept override;
	HRESULT __stdcall CreateVertexShader( CONST DWORD *pFunction, IDirect3DVertexShader9 **ppShader ) noexcept override;
	HRESULT __stdcall SetVertexShader( IDirect3DVertexShader9 *pShader ) noexcept override;
	HRESULT __stdcall GetVertexShader( IDirect3DVertexShader9 **ppShader ) noexcept override;
	HRESULT __stdcall SetVertexShaderConstantF( UINT StartRegister, CONST float *pConstantData, UINT Vector4fCount ) noexcept override;
	HRESULT __stdcall GetVertexShaderConstantF( UINT StartRegister, float *pConstantData, UINT Vector4fCount ) noexcept override;
	HRESULT __stdcall SetVertexShaderConstantI( UINT StartRegister, CONST int *pConstantData, UINT Vector4iCount ) noexcept override;
	HRESULT __stdcall GetVertexShaderConstantI( UINT StartRegister, int *pConstantData, UINT Vector4iCount ) noexcept override;
	HRESULT __stdcall SetVertexShaderConstantB( UINT StartRegister, CONST BOOL *pConstantData, UINT BoolCount ) noexcept override;
	HRESULT __stdcall GetVertexShaderConstantB( UINT StartRegister, BOOL *pConstantData, UINT BoolCount ) noexcept override;
	HRESULT __stdcall SetStreamSource( UINT StreamNumber,
									   IDirect3DVertexBuffer9 *pStreamData,
									   UINT OffsetInBytes,
									   UINT Stride ) noexcept override;
	HRESULT __stdcall GetStreamSource( UINT StreamNumber,
									   IDirect3DVertexBuffer9 **ppStreamData,
									   UINT *OffsetInBytes,
									   UINT *pStride ) noexcept override;
	HRESULT __stdcall SetStreamSourceFreq( UINT StreamNumber, UINT Divider ) noexcept override;
	HRESULT __stdcall GetStreamSourceFreq( UINT StreamNumber, UINT *Divider ) noexcept override;
	HRESULT __stdcall SetIndices( IDirect3DIndexBuffer9 *pIndexData ) noexcept override;
	HRESULT __stdcall GetIndices( IDirect3DIndexBuffer9 **ppIndexData ) noexcept override;
	HRESULT __stdcall CreatePixelShader( CONST DWORD *pFunction, IDirect3DPixelShader9 **ppShader ) noexcept override;
	HRESULT __stdcall SetPixelShader( IDirect3DPixelShader9 *pShader ) noexcept override;
	HRESULT __stdcall GetPixelShader( IDirect3DPixelShader9 **ppShader ) noexcept override;
	HRESULT __stdcall SetPixelShaderConstantF( UINT StartRegister, CONST float *pConstantData, UINT Vector4fCount ) noexcept override;
	HRESULT __stdcall GetPixelShaderConstantF( UINT StartRegister, float *pConstantData, UINT Vector4fCount ) noexcept override;
	HRESULT __stdcall SetPixelShaderConstantI( UINT StartRegister, CONST int *pConstantData, UINT Vector4iCount ) noexcept override;
	HRESULT __stdcall GetPixelShaderConstantI( UINT StartRegister, int *pConstantData, UINT Vector4iCount ) noexcept override;
	HRESULT __stdcall SetPixelShaderConstantB( UINT StartRegister, CONST BOOL *pConstantData, UINT BoolCount ) noexcept override;
	HRESULT __stdcall GetPixelShaderConstantB( UINT StartRegister, BOOL *pConstantData, UINT BoolCount ) noexcept override;
	HRESULT __stdcall DrawRectPatch( UINT Handle, CONST float *pNumSegs, CONST D3DRECTPATCH_INFO *pRectPatchInfo ) noexcept override;
	HRESULT __stdcall DrawTriPatch( UINT Handle, CONST float *pNumSegs, CONST D3DTRIPATCH_INFO *pTriPatchInfo ) noexcept override;
	HRESULT __stdcall DeletePatch( UINT Handle ) noexcept override;
	HRESULT __stdcall CreateQuery( D3DQUERYTYPE Type, IDirect3DQuery9 **ppQuery ) noexcept override;

public:
#	if __has_include( <SRSignal/SRSignal.hpp>)
	SRSignal<REFIID, void **> onQueryInterface;
	SRSignal<> onAddRef;
	SRSignal<> onRelease;
	SRSignal<> onTestCooperativeLevel;
	SRSignal<> onGetAvailableTextureMem;
	SRSignal<> onEvictManagedResources;
	SRSignal<IDirect3D9 **> onGetDirect3D;
	SRSignal<D3DCAPS9 *> onGetDeviceCaps;
	SRSignal<UINT, D3DDISPLAYMODE *> onGetDisplayMode;
	SRSignal<D3DDEVICE_CREATION_PARAMETERS *> onGetCreationParameters;
	SRSignal<UINT, UINT, IDirect3DSurface9 *> onSetCursorProperties;
	SRSignal<int, int, DWORD> onSetCursorPosition;
	SRSignal<BOOL> onShowCursor;
	SRSignal<D3DPRESENT_PARAMETERS *, IDirect3DSwapChain9 **> onCreateAdditionalSwapChain;
	SRSignal<UINT, IDirect3DSwapChain9 **> onGetSwapChain;
	SRSignal<> onGetNumberOfSwapChains;
	SRSignal<> onPreReset;
	SRSignal<D3DPRESENT_PARAMETERS *> onReset;
	SRSignal<> onPostReset;
	SRSignal<HRESULT> onPostResetFail;
	SRSignal<CONST RECT *, CONST RECT *, HWND, CONST RGNDATA *> onPresent;
	SRSignal<> onDraw;
	SRSignal<UINT, UINT, D3DBACKBUFFER_TYPE, IDirect3DSurface9 **> onGetBackBuffer;
	SRSignal<UINT, D3DRASTER_STATUS *> onGetRasterStatus;
	SRSignal<BOOL> onSetDialogBoxMode;
	SRSignal<UINT, DWORD, CONST D3DGAMMARAMP *> onSetGammaRamp;
	SRSignal<UINT, D3DGAMMARAMP *> onGetGammaRamp;
	SRSignal<UINT, UINT, UINT, DWORD, D3DFORMAT, D3DPOOL, IDirect3DTexture9 **, HANDLE *> onCreateTexture;
	SRSignal<UINT, UINT, UINT, UINT, DWORD, D3DFORMAT, D3DPOOL, IDirect3DVolumeTexture9 **, HANDLE *> onCreateVolumeTexture;
	SRSignal<UINT, UINT, DWORD, D3DFORMAT, D3DPOOL, IDirect3DCubeTexture9 **, HANDLE *> onCreateCubeTexture;
	SRSignal<UINT, DWORD, DWORD, D3DPOOL, IDirect3DVertexBuffer9 **, HANDLE *> onCreateVertexBuffer;
	SRSignal<UINT, DWORD, D3DFORMAT, D3DPOOL, IDirect3DIndexBuffer9 **, HANDLE *> onCreateIndexBuffer;
	SRSignal<UINT, UINT, D3DFORMAT, D3DMULTISAMPLE_TYPE, DWORD, BOOL, IDirect3DSurface9 **, HANDLE *> onCreateRenderTarget;
	SRSignal<UINT, UINT, D3DFORMAT, D3DMULTISAMPLE_TYPE, DWORD, BOOL, IDirect3DSurface9 **, HANDLE *> onCreateDepthStencilSurface;
	SRSignal<IDirect3DSurface9 *, CONST RECT *, IDirect3DSurface9 *, CONST POINT *> onUpdateSurface;
	SRSignal<IDirect3DBaseTexture9 *, IDirect3DBaseTexture9 *> onUpdateTexture;
	SRSignal<IDirect3DSurface9 *, IDirect3DSurface9 *> onGetRenderTargetData;
	SRSignal<UINT, IDirect3DSurface9 *> onGetFrontBufferData;
	SRSignal<IDirect3DSurface9 *, CONST RECT *, IDirect3DSurface9 *, CONST RECT *, D3DTEXTUREFILTERTYPE> onStretchRect;
	SRSignal<IDirect3DSurface9 *, CONST RECT *, D3DCOLOR> onColorFill;
	SRSignal<UINT, UINT, D3DFORMAT, D3DPOOL, IDirect3DSurface9 **, HANDLE *> onCreateOffscreenPlainSurface;
	SRSignal<DWORD, IDirect3DSurface9 *> onSetRenderTarget;
	SRSignal<DWORD, IDirect3DSurface9 **> onGetRenderTarget;
	SRSignal<IDirect3DSurface9 *> onSetDepthStencilSurface;
	SRSignal<IDirect3DSurface9 **> onGetDepthStencilSurface;
	SRSignal<> onBeginScene;
	SRSignal<> onEndScene;
	SRSignal<DWORD, CONST D3DRECT *, DWORD, D3DCOLOR, float, DWORD> onClear;
	SRSignal<D3DTRANSFORMSTATETYPE, CONST D3DMATRIX *> onSetTransform;
	SRSignal<D3DTRANSFORMSTATETYPE, D3DMATRIX *> onGetTransform;
	SRSignal<D3DTRANSFORMSTATETYPE, CONST D3DMATRIX *> onMultiplyTransform;
	SRSignal<CONST D3DVIEWPORT9 *> onSetViewport;
	SRSignal<D3DVIEWPORT9 *> onGetViewport;
	SRSignal<CONST D3DMATERIAL9 *> onSetMaterial;
	SRSignal<D3DMATERIAL9 *> onGetMaterial;
	SRSignal<DWORD, CONST D3DLIGHT9 *> onSetLight;
	SRSignal<DWORD, D3DLIGHT9 *> onGetLight;
	SRSignal<DWORD, BOOL> onLightEnable;
	SRSignal<DWORD, BOOL *> onGetLightEnable;
	SRSignal<DWORD, CONST float *> onSetClipPlane;
	SRSignal<DWORD, float *> onGetClipPlane;
	SRSignal<D3DRENDERSTATETYPE, DWORD> onSetRenderState;
	SRSignal<D3DRENDERSTATETYPE, DWORD *> onGetRenderState;
	SRSignal<D3DSTATEBLOCKTYPE, IDirect3DStateBlock9 **> onCreateStateBlock;
	SRSignal<> onBeginStateBlock;
	SRSignal<IDirect3DStateBlock9 **> onEndStateBlock;
	SRSignal<CONST D3DCLIPSTATUS9 *> onSetClipStatus;
	SRSignal<D3DCLIPSTATUS9 *> onGetClipStatus;
	SRSignal<DWORD, IDirect3DBaseTexture9 **> onGetTexture;
	SRSignal<DWORD, IDirect3DBaseTexture9 *> onSetTexture;
	SRSignal<DWORD, D3DTEXTURESTAGESTATETYPE, DWORD *> onGetTextureStageState;
	SRSignal<DWORD, D3DTEXTURESTAGESTATETYPE, DWORD> onSetTextureStageState;
	SRSignal<DWORD, D3DSAMPLERSTATETYPE, DWORD *> onGetSamplerState;
	SRSignal<DWORD, D3DSAMPLERSTATETYPE, DWORD> onSetSamplerState;
	SRSignal<DWORD *> onValidateDevice;
	SRSignal<UINT, CONST PALETTEENTRY *> onSetPaletteEntries;
	SRSignal<UINT, PALETTEENTRY *> onGetPaletteEntries;
	SRSignal<UINT> onSetCurrentTexturePalette;
	SRSignal<UINT *> onGetCurrentTexturePalette;
	SRSignal<CONST RECT *> onSetScissorRect;
	SRSignal<RECT *> onGetScissorRect;
	SRSignal<BOOL> onSetSoftwareVertexProcessing;
	SRSignal<> onGetSoftwareVertexProcessing;
	SRSignal<float> onSetNPatchMode;
	SRSignal<> onGetNPatchMode;
	SRSignal<D3DPRIMITIVETYPE, UINT, UINT> onDrawPrimitive;
	SRSignal<D3DPRIMITIVETYPE, INT, UINT, UINT, UINT, UINT> onDrawIndexedPrimitive;
	SRSignal<D3DPRIMITIVETYPE, UINT, CONST void *, UINT> onDrawPrimitiveUP;
	SRSignal<D3DPRIMITIVETYPE, UINT, UINT, UINT, CONST void *, D3DFORMAT, CONST void *, UINT> onDrawIndexedPrimitiveUP;
	SRSignal<UINT, UINT, UINT, IDirect3DVertexBuffer9 *, IDirect3DVertexDeclaration9 *, DWORD> onProcessVertices;
	SRSignal<CONST D3DVERTEXELEMENT9 *, IDirect3DVertexDeclaration9 **> onCreateVertexDeclaration;
	SRSignal<IDirect3DVertexDeclaration9 *> onSetVertexDeclaration;
	SRSignal<IDirect3DVertexDeclaration9 **> onGetVertexDeclaration;
	SRSignal<DWORD> onSetFVF;
	SRSignal<DWORD *> onGetFVF;
	SRSignal<CONST DWORD *, IDirect3DVertexShader9 **> onCreateVertexShader;
	SRSignal<IDirect3DVertexShader9 *> onSetVertexShader;
	SRSignal<IDirect3DVertexShader9 **> onGetVertexShader;
	SRSignal<UINT, CONST float *, UINT> onSetVertexShaderConstantF;
	SRSignal<UINT, float *, UINT> onGetVertexShaderConstantF;
	SRSignal<UINT, CONST int *, UINT> onSetVertexShaderConstantI;
	SRSignal<UINT, int *, UINT> onGetVertexShaderConstantI;
	SRSignal<UINT, CONST BOOL *, UINT> onSetVertexShaderConstantB;
	SRSignal<UINT, BOOL *, UINT> onGetVertexShaderConstantB;
	SRSignal<UINT, IDirect3DVertexBuffer9 *, UINT, UINT> onSetStreamSource;
	SRSignal<UINT, IDirect3DVertexBuffer9 **, UINT *, UINT *> onGetStreamSource;
	SRSignal<UINT, UINT> onSetStreamSourceFreq;
	SRSignal<UINT, UINT *> onGetStreamSourceFreq;
	SRSignal<IDirect3DIndexBuffer9 *> onSetIndices;
	SRSignal<IDirect3DIndexBuffer9 **> onGetIndices;
	SRSignal<CONST DWORD *, IDirect3DPixelShader9 **> onCreatePixelShader;
	SRSignal<IDirect3DPixelShader9 *> onSetPixelShader;
	SRSignal<IDirect3DPixelShader9 **> onGetPixelShader;
	SRSignal<UINT, CONST float *, UINT> onSetPixelShaderConstantF;
	SRSignal<UINT, float *, UINT> onGetPixelShaderConstantF;
	SRSignal<UINT, CONST int *, UINT> onSetPixelShaderConstantI;
	SRSignal<UINT, int *, UINT> onGetPixelShaderConstantI;
	SRSignal<UINT, CONST BOOL *, UINT> onSetPixelShaderConstantB;
	SRSignal<UINT, BOOL *, UINT> onGetPixelShaderConstantB;
	SRSignal<UINT, CONST float *, CONST D3DRECTPATCH_INFO *> onDrawRectPatch;
	SRSignal<UINT, CONST float *, CONST D3DTRIPATCH_INFO *> onDrawTriPatch;
	SRSignal<UINT> onDeletePatch;
	SRSignal<D3DQUERYTYPE, IDirect3DQuery9 **> onCreateQuery;
#	endif
	IDirect3DDevice9 *d3d9_device();

	void __stdcall d3d9_hook( HRESULT ret );
	void __stdcall d3d9_hook( ULONG ret );
	void __stdcall d3d9_hook( UINT ret );
	void __stdcall d3d9_hook( BOOL ret );
	void __stdcall d3d9_hook( float ret );

#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	class CD3DFont *
		d3d9_createFont( std::string_view fontName = "", int fontHeight = 15, DWORD dwCreateFlags = 4, DWORD dwCharSet = DEFAULT_CHARSET );
	class CD3DRender *d3d9_createRender( int numVertices = 128 );
	class SRTexture *d3d9_createTexture( int width, int height );

	void __stdcall d3d9_registerFont( class CD3DFont *pFont );
	void __stdcall d3d9_releaseFont( class CD3DFont *pFont, bool autoDelete = true );
	void __stdcall d3d9_registerRender( class CD3DRender *pRender );
	void __stdcall d3d9_releaseRender( class CD3DRender *pRender, bool autoDelete = true );
	void __stdcall d3d9_registerTexture( class SRTexture *pTexture );
	void __stdcall d3d9_releaseTexture( class SRTexture *pTexture, bool autoDelete = true );
#	endif

	void __stdcall d3d9_hook( uIDirect3DDevice9 ret );
	bool __stdcall d3d9_hasHooked();

	bool __stdcall d3d9_destroy() noexcept;

protected:
	bool __stdcall isHooked();
	void __stdcall resetHooked();

private:
	IDirect3DDevice9 *origIDirect3DDevice9 = static_cast<IDirect3DDevice9 *>( nullptr );
#	if __has_include( <render/d3drender.h>) && __has_include(<render/texture.h>)
	std::deque<class CD3DFont *> fontList;
	std::deque<class CD3DRender *> renderList;
	std::deque<class SRTexture *> textureList;
#	endif
	uIDirect3DDevice9 _hookResult;
	bool _isHook;
};
#else
static_assert( false, "Can't include <d3d9.h>" );
#endif
